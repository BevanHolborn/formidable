FROM ruby:3.4.1

RUN apt-get update && apt-get upgrade -y && apt-get install -y \
    build-essential \
	nano

ARG APPLICATION_IDENTIFIER

ARG USER_ID
ARG GROUP_ID

RUN addgroup --gid $GROUP_ID $APPLICATION_IDENTIFIER
RUN adduser --disabled-password --gecos '' --uid $USER_ID --gid $GROUP_ID $APPLICATION_IDENTIFIER
RUN chown -R $USER_ID:$GROUP_ID /opt
USER $APPLICATION_IDENTIFIER

WORKDIR /opt/$APPLICATION_IDENTIFIER

# Setup bundler
RUN gem install bundler -v 2.6.2
