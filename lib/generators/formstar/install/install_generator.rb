require "rails/generators"

module Formstar
  module Generators
    class InstallGenerator < Rails::Generators::Base
      source_root File.expand_path("templates", __dir__)

      def copy_application_enum
        template "app/forms/application_form.rb", "app/forms/application_form.rb"
      end

    end
  end
end