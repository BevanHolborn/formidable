class ApplicationForm < Formstar::Base
  model_translation_fallback true
  submit_within_transaction true
end