require "after_commit_everywhere"
require "active_model"
require "active_support"

module Formstar
  class Base
    include ActiveModel::Model
    include ActiveModel::Attributes
    include ActiveModel::Validations::Callbacks
    include AfterCommitEverywhere

    class NoModelError < StandardError; end

    define_model_callbacks :initialize, only: [:after]
    define_model_callbacks :save, only: [:after]
    define_model_callbacks :commit, only: [:after]

    attr_reader :form_succeeded
    attr_reader :form_submitted

    validate :validate_form_model, -> { form_model_defined? && !form_model.nil? }

    def initialize(...)
      raise NotImplementedError.new("`#{self.class.name}` is abstract and cannot be instantiated directly.") if ["Formstar::Base", "ApplicationForm"].include?(self.class.name)

      @form_succeeded = nil
      @form_submitted = false

      run_callbacks(:initialize) do
        super(...)
      end
    end

    def submit
      raise NotImplementedError
    end

    def succeeded?
      @form_succeeded
    end

    def submitted?
      @form_submitted
    end

    def form_attributes
      return @form_attributes unless @form_attributes.nil?

      @form_attributes = {}
      self.class.form_attribute_names.each do |attr_name|
        @form_attributes[attr_name] = send(attr_name)
      end

      @form_attributes
    end

    def save(**opts)
      save!(**opts)
    rescue
      false
    end

    def save!(**opts)
      @form_submitted = true
      validate!(context: opts[:context]) unless opts[:validate] == false

      perform_submit = -> do
        run_callbacks(:save) do
          submit
        end
      end

      if self.class.config[:submit_within_transaction_enabled]
        ActiveRecord::Base.transaction do
          perform_submit.call
        end
      else
        perform_submit.call
      end

      after_commit { run_callbacks(:commit) }

      @form_succeeded = true
    rescue => e
      @form_succeeded = false
      raise e
    end

    def model
      raise NoModelError.new unless form_model_defined?
      self.form_model
    end

    def model=(new_model)
      raise NoModelError.new unless form_model_defined?
      self.form_model = new_model
    end

    private

    def form_model
      return nil unless form_model_defined?
      send(form_model_name)
    end

    def form_model=(new_model)
      return nil unless form_model_defined?
      send("#{form_model_name}=".to_sym, new_model)
    end

    def validate_form_model
      promote_form_model_errors(form_model) if form_model&.invalid?
    end

    def promote_form_model_errors(form_model)
      form_model.errors.each do |e|
        errors.add(e.attribute, e.type, message: e.message)
      end
    end

    def form_model_name
      @form_model_name ||= self.class.config[:model].fetch(:name, nil)&.to_sym
    end

    def form_model_defined?
      @model_defined ||= !self.class.config[:model].nil? #&&
    end

    def config
      self.class.config
    end

    def respond_to_missing?(name, include_private = false)
      if form_model_defined? && name.to_sym != self.class.config[:model][:name]
        form_model.respond_to?(name, include_private)
      else
        super
      end
    end

    def method_missing(method, *args, &block)
      if form_model_defined? && method.to_sym != self.class.config[:model][:name]
        form_model.send(method, *args, &block)
      else
        super
      end
    end

    class << self

      attr_reader :config
      attr_reader :form_attribute_names

      def inherited(subclass)
        subclass.instance_variable_set(:@config, self.config.dup)
      end

      def config
        @config ||= {
          model_translation_fallback_enabled: true,
          model_translation_fallback_class_name: nil,
          submit_within_transaction_enabled: true,
          model: nil
        }
      end

      def model_translation_fallback(enabled, class_name: nil)
        config[:model_translation_fallback_enabled] = enabled
        config[:model_translation_fallback_class_name] = class_name&.safe_constantize
      end

      def submit_within_transaction(enabled)
        config[:submit_within_transaction_enabled] = enabled
      end

      def attr_form(name, ...)
        form_attribute_names.add(name.to_sym)
        attribute(name, ...)
      end

      def with_model(name, class_name: name)
        name = name.to_sym
        class_name = class_name.to_s.classify
        klass = Object.const_get(class_name)

        # Don't accept models that are modules.
        raise ArgumentError.new("Expected an ActiveRecord model, got `#{class_name}`") unless klass < ActiveRecord::Base

        config[:model] = { name: name, klass: klass }
      end

      def model_name
        return @model_name unless @model_name.nil?

        if config[:model]
          @model_name = ActiveModel::Name.new(config[:model][:klass])
          @model_name.i18n_key = self.name.underscore
        else
          @model_name = ActiveModel::Name.new(self)
        end

        @model_name
      end

      def form_attribute_names
        @form_attribute_names ||= Set.new
      end

      def i18n_scope
        :form
      end

      def human_attribute_name(attribute, options = {})
        # Base errors for a form will never fall back to the underlying model.
        # So we can safely always delegate the translation lookup back to active model
        # which will build up the correct lookup paths.
        return attribute if attribute.to_sym == :base

        I18n.t!("#{i18n_scope}.attributes.#{model_name.i18n_key}.#{attribute}")
      rescue I18n::MissingTranslationData => e
        fallback_value = nil

        if config[:model_translation_fallback_enabled]
          if config[:model]
            form_model_klass = config[:model][:klass]
          else
            form_model_klass = nil
          end

          fallback_klass = config[:model_translation_fallback_class_name] || form_model_klass
          fallback_value = fallback_klass.human_attribute_name(attribute, options) if fallback_klass
        end

        if fallback_value
          fallback_value
        else
          raise e
        end
      end
    end

  end
end