require 'models/user'
require 'forms/user/new_form'
require 'forms/session/reset_password_form'

RSpec.describe Formstar::Base do

  it 'includes the correct modules' do
    expect(described_class.ancestors).to include(ActiveModel::Model)
    expect(described_class.ancestors).to include(ActiveModel::Validations::Callbacks)
    expect(described_class.ancestors).to include(AfterCommitEverywhere)
  end

  it 'registers the correct callbacks' do
    expect(described_class.respond_to?(:after_initialize)).to eq true
    expect(described_class.respond_to?(:after_save)).to eq true
    expect(described_class.respond_to?(:after_commit)).to eq true
  end

  it 'does not instantiate the abstract base class' do
    expect { described_class.new }.to raise_error(NotImplementedError)
  end

  it 'runs initialize callbacks after initialization' do
    expect_any_instance_of(User::NewForm).to receive(:set_defaults)
    User::NewForm.new(user: User.new)
  end

  describe '#model' do
    describe 'when there is no model' do
      it 'it returns nil' do
        form = Session::ResetPasswordForm.new
        expect(form.model).to be_nil
      end
    end
  end

  describe '#model=' do
    describe 'when there is no model' do
      it 'it does nothing' do
        form = Session::ResetPasswordForm.new
        old_value = form.model
        form.model = :test_value
        new_value = form.model
        expect(old_value == new_value).to eq true
      end
    end
  end

  describe 'when delegating methods to the model object' do
    describe 'when there is no model' do
      it 'raises an error' do
        form = Session::ResetPasswordForm.new
        expect { form.nonexistant_method }.to raise_error(NoMethodError)
      end
    end

    describe 'when there is a model' do

      before :each do
        @form = User::NewForm.new(user: User.new)
      end

      describe 'and it responds to the method' do
        it 'performs the method' do
          expect(@form.existing_user_method).to eq :test_value
        end
      end

      describe 'and it does not respond to the method' do
        it 'raises an error' do
          expect { @form.nonexistant_method }.to raise_error(NoMethodError)
        end
      end
    end
  end

end