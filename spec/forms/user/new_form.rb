class User::NewForm < Formstar::Base

  attr_accessor :user

  with_model :user, class_name: "User"

  after_initialize :set_defaults

  def submit

  end

  def set_defaults
    true
  end

end