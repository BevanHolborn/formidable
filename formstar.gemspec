# frozen_string_literal: true

require_relative "lib/formstar/version"

Gem::Specification.new do |spec|
  spec.name = "formstar"
  spec.version = Formstar::VERSION
  spec.authors = ["Bevan Holborn"]
  spec.description = "ActiveModel backed Form objects for Ruby on Rails applications"
  spec.summary = "ActiveModel backed Form objects for Ruby on Rails applications"
  spec.homepage = "https://rubygems.org/gems/formstar"
  spec.license = "MIT"
  spec.metadata = { "source_code_uri" => "https://gitlab.com/BevanHolborn/formstar" }

  spec.files = Dir["lib/**/*.rb"]
  spec.require_paths = ["lib"]

  spec.required_ruby_version = '>= 2.7.0'

  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec', '>= 3.0'

  spec.add_runtime_dependency "after_commit_everywhere", '>= 1.4'
  spec.add_runtime_dependency "activemodel", '>= 5.0'
  spec.add_runtime_dependency "activesupport", '>= 5.0'
end
