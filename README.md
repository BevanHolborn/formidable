# Formstar

ActiveModel backed Form objects for Ruby on Rails applications

**Formstar** is designed to handle form processing in your applications. It provides an easy-to-use interface for 
managing forms with model-like behavior, including support for callbacks, error handling and translations.

## Installation
```ruby
bundle add formstar
````

Run the generator, which will set up an application enum
```ruby
rails generate formstar:install
```

## Usage
To use Formstar, Create a class (usually in `app/forms`) that inherits from ApplicationForm.

```ruby
class RegisterForm < ApplicationForm

  attr_form :email
  attr_form :username
  attr_form :password
  
  attribute :user
  
  with_model :user
  
  validates :email, presence: true
  validates :username, presence: true
  validates :password, presence: true
  
  def submit
    user = User.new(**form_attributes)
    user.save!
  end
  
end
```

A form's lifecycle will usually start in a controller action:

```ruby
def register
  @register_form = RegisterForm.new(**register_params)
  
  if @register_form.save
    redirect_to user_path(id: @form.user.id)
  else
    render 'users/register'
  end
end
```

Let's breakdown how this form works.

Note: Forms are backed by [ActiveModel](https://guides.rubyonrails.org/active_model_basics.html). How this can be 
leveraged will be explored in the following sections.

### Attributes
A form can define form attributes with `attr_form`, which uses ActiveModel's `attribute` behind the scene, and supports all of its parameters.

It's recommended that you use `attr_form` for attributes received from the controller, and `attribute` for other attributes. 
However, you can use regular ruby `attr_*`, methods as well.

### Submissions
Forms must define the `submit` method, which is called when `save` is called on a form instance. 
Before `submit` is executed, validations are run and `submit` is called only if validations pass (This also means you have access to errors on the form). 

`save` behaves like ActiveRecord's `save` method, returning a boolean representing if submitting succeeded or not. 
Forms also support `save!` which will raise an error if the form does not submit correctly. 

By default, `submit` runs within a transaction, this can be disabled by adding the following line to your form:
```ruby
class RegisterForm < ApplicationForm
  submit_within_transaction false
end
```

### Callbacks
Forms also support the following callbacks:

- `after_initialize`
- `before_validation`
- `after_validation`
- `after_commit` (Possible outside of ActiveRecord with [after_commit_everywhere](https://github.com/Envek/after_commit_everywhere))

### Submission state & form attributes
The following methods exist to see the submission status of a form:

- `submitted?` Returns a boolean indicating if the form has been submitted or not.
- `succeeded?` Returns a boolean indicating if the form's submission was successful or not, and will return `nil` if the form has not yet been submitted.

All form attributes defined with `attr_form` can be accessed with `form_attributes` which will return the attributes in a hash:
```ruby
@register_form.form_attrtibutes # => { email: "...", username: "...", password: "..." }
```

### Translations
Forms support translations for their attributes. Using the `username` attribute as an example, it's translation lookup path will be:
```
form.attributes.register_form.username
```

If `username` was validated as `blank`, it's error translation lookup path will be:
```
form.errors.register_form.attributes.username
```

### Model
You can associate your form with an ActiveRecord model using `with_model`. It requires the name of a form attribute, e.g `with_model: :user`. The form will try to infer the class of 
the attribute. You can specify the class with the `:class_name` parameter, e.g `with_model: :user, class_name: "User"`.

Associating a model with your form allows you to leverage information about
your model and it's class within the form:

#### Translation fallbacks
A lot of the time, your form will define attributes that match those of the model. The user will also have 
`email`, `username` and `password` attributes. If no translation is found for a given attribute, the form will attempt to fallback onto the model's translations.

For example, if no translation was found at `form.attributes.register_form.username`, then it will look up a translation at 
`activerecord.attributes.user.username`.

By default, model fallback translations are enabled, this can be disabled by adding the following line to your form:
```ruby
class RegisterForm < ApplicationForm
  model_translation_fallback false
end
```

If, for some reason, you want to fall back to a different model, you can specify the fallback class:
```ruby
class RegisterForm < ApplicationForm
  model_translation_fallback true, class_name: "Account"
end
```

#### Attribute access
You can access attributes from the model directly from the form. E.g `@register_form.id` will return the users ID. 
Sometimes you won't be able to access the value this way, as the form has defined an attribute of the same name. In that
case you can access the value directly through the model with `@register_form.model.id`

#### Form builder integration
When you associate a model with your form, it makes use of information from the model to allow you to integrate with form builders.

```
<%= form_with model: @register_form do |f|
    <%= f.label :username %>
    <%= f.text_field :username %>
    
    <%= f.label :email %>
    <%= f.email_field :email %>
    
    <%= f.label :password %>
    <%= f.password_field :password %>
    
    <%= f.submit %>
<% end %>
```

The form builder will automatically:
- Infer the correct route to submit the form, e.g `users_path` (If a new record) or `user_path(id: 1)` (If an existing record).
- Populate the fields with attributes defined on the form, or fallback to values from the model.
- Use translation fallbacks for the model (if enabled).
- Identify fields from the form and the model that have validation errors. This is accomplished by promoting any validation 
errors from the model to the form.